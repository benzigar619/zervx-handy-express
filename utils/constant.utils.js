const utils = {};

// Categories
utils.MAIN = 'main';
utils.SUB = 'sub';
utils.TEXTBOX = 'TEXTBOX';

// CRONS
utils.CRON_EVERY_MINUTE = '* * * * *';
utils.CRON_EVERY_FIVE_MINUTE = '*/5 * * * *';
utils.CRON_EVERY_FIFTEEN_MINUTE = '*/15 * * * *';
utils.CRON_EVERY_THIRTY_MINUTE = '*/30 * * * *';
utils.CRON_EVERY_HOUR = '0 * * * *';
utils.CRON_EVERY_DAY = '0 0 * * *';

module.exports = utils;
