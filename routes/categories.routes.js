const express = require('express');

const controller = require('../controllers/categories.controller');
const validations = require('../validations/categories.validation');
const { validateBody } = require('../utils/errors.utils');

const router = express.Router();

router.post('/addCategory', validateBody(validations.addCategories), controller.addCategory);
router.post('/getAllCategories', controller.getAllCategories);
router.post('/getSubCategories', validateBody(validations.getCategory), controller.getSubCategories);
router.post('/getCategory', validateBody(validations.getCategory), controller.getCategory);
router.post('/check', controller.check);

module.exports = router;
