const config = require('./config');
const clientCheck = () => {
    console.log('From Admin to Clients');
    const io = require('socket.io-client');
    const notificationSocket = io(
        `ws://localhost:${config.SOCKET_PORT}/notifications`,
        {
            transports: ['websocket'],
            query: {
                type: '',
            },
        }
    );
    notificationSocket.on('con', (data) => console.log(data));
    notificationSocket.onAny((event, ...args) => {
        console.log(`Notification Socket : ${event}`);
        console.log(args[0]);
    });
};

clientCheck();

module.exports = clientCheck;
