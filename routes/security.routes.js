const express = require('express');
const controller = require('../controllers/security.controller');
const router = express.Router();

router.get('/getSecurityDetails', controller.getSecurityDetails);

module.exports = router;
