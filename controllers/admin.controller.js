const handler = require('express-async-handler');
const { upload } = require('../utils/upload.utils');

const Admin = require('../models/admin.models');
const redis = require('../utils/redis.utils');

const socket = require('../utils/socket.utils');

const controller = {};

controller.generateSearch = (fields, search) => {
    let final = [];
    fields.map((each) => {
        let temp = {};
        temp[each] = {
            $regex: search || '',
            $options: 'i',
        };
        final.push(temp);
    });
    return final;
};

controller.adminListPaginate = ({
    name = '',
    searchCond = {},
    project = {},
    skip = 0,
    limit = 10,
}) => {
    return new Promise(async (resolve, reject) => {
        try {
            const final = await Admin.aggregate([
                {
                    $facet: {
                        count: [
                            {
                                $match: {
                                    name: name,
                                    $or: searchCond,
                                },
                            },
                            { $count: 'all' },
                        ],
                        data: [
                            {
                                $match: {
                                    name: name,
                                    $or: searchCond,
                                },
                            },
                            { $sort: { createdAt: -1 } },
                            { $skip: parseInt(skip) },
                            { $limit: parseInt(limit) },
                            { $project: project },
                        ],
                    },
                },
            ]);
            resolve({
                count: final[0]?.count[0]?.all || 0,
                data: final[0]?.data || [],
            });
        } catch (err) {
            reject(err);
        }
    });
};

controller.getAllNames = handler(async (req, res) => {
    const names = await Admin.find({}, { name: 1 });
    socket.notificationSocket.emit('getAllNames', [
        ...new Set(names.map((each) => each.name)),
    ]);
    res.json([...new Set(names.map((each) => each.name))]);
});

controller.getConfig = async (req, res) => {
    const config = await Admin.findOne({ name: 'GENERALSETTING' });
    // const config = await redis.get('GENERALSETTING');
    res.json(config.data);
};

controller.getAllOperators = async (req, res) => {
    const searchCond = controller.generateSearch(
        ['data.firstName', 'data.lastName', 'data.email', 'data.phone.number'],
        req.body.search || ''
    );
    const project = {
        _id: 1,
        firstName: '$data.firstName',
        lastName: '$data.lastName',
        email: '$data.email',
        gender: '$data.gender',
        status: '$data.status',
        phoneCode: '$data.phone.code',
        phoneNumber: '$data.phone.number',
    };
    const operators = await controller.adminListPaginate({
        name: 'OPERATORS',
        searchCond,
        project,
        skip: req.body.skip || 0,
        limit: req.body.limit || 10,
    });
    return res.json(operators);
};

controller.getAllDevelopers = async (req, res) => {
    const searchCond = controller.generateSearch(
        ['data.firstName', 'data.lastName', 'data.email', 'data.phone.number'],
        req.body.search || ''
    );
    const project = {
        _id: 1,
        firstName: '$data.firstName',
        lastName: '$data.lastName',
        email: '$data.email',
        gender: '$data.gender',
        status: '$data.status',
        phoneCode: '$data.phone.code',
        phoneNumber: '$data.phone.number',
    };
    const operators = await controller.adminListPaginate({
        name: 'DEVELOPER',
        searchCond,
        project,
        skip: req.body.skip || 0,
        limit: req.body.limit || 10,
    });
    res.json(operators);
};

controller.getAllAdmin = async (req, res) => {
    const searchCond = controller.generateSearch(
        ['data.firstName', 'data.lastName', 'data.email', 'data.phone.number'],
        req.body.search || ''
    );
    const project = {
        _id: 1,
        firstName: '$data.firstName',
        lastName: '$data.lastName',
        email: '$data.email',
        gender: '$data.gender',
        status: '$data.status',
        phoneCode: '$data.phone.code',
        phoneNumber: '$data.phone.number',
    };
    const operators = await controller.adminListPaginate({
        name: 'ADMIN',
        searchCond,
        project,
        skip: req.body.skip || 0,
        limit: req.body.limit || 10,
    });
    res.json(operators);
};

controller.getAllHubs = async (req, res) => {
    const searchCond = controller.generateSearch(
        ['data.firstName', 'data.lastName', 'data.email', 'data.phone.number'],
        req.body.search || ''
    );
    const project = {
        _id: 1,
        firstName: '$data.firstName',
        lastName: '$data.lastName',
        email: '$data.email',
        gender: '$data.gender',
        status: '$data.status',
        phoneCode: '$data.phone.code',
        phoneNumber: '$data.phone.number',
    };
    const operators = await controller.adminListPaginate({
        name: 'HUBS',
        searchCond,
        project,
        skip: req.body.skip || 0,
        limit: req.body.limit || 10,
    });
    res.json(operators);
};

controller.getAllLanguages = async (req, res) => {
    const searchCond = controller.generateSearch(
        ['data.languageName', 'data.languageCode'],
        req.body.search || ''
    );
    const project = {
        _id: 1,
        languageName: '$data.languageName',
        languageCode: '$data.languageCode',
        languageDirection: '$data.languageDirection',
        default: '$data.languageDefault',
    };
    const operators = await controller.adminListPaginate({
        name: 'LANGUAGES',
        searchCond,
        project,
        skip: req.body.skip || 0,
        limit: req.body.limit || 10,
    });
    res.json(operators);
};

controller.getAllDocuments = async (req, res) => {
    const searchCond = controller.generateSearch(
        ['data.docsName', 'data.docsDetail'],
        req.body.search || ''
    );
    const project = {
        _id: 1,
        docsName: '$data.docsName',
        docsFor: '$data.docsFor',
        docsDetail: '$data.docsDetail',
        docsReferImage: '$data.docsReferImage',
        docsMandatory: '$data.docsMandatory',
        docsExpiry: '$data.docsExpiry',
        status: '$data.status',
    };
    const operators = await controller.adminListPaginate({
        name: 'DOCUMENTS',
        searchCond,
        project,
        skip: req.body.skip || 0,
        limit: req.body.limit || 10,
    });
    res.json(operators);
};

controller.imageUpload = handler(async (req, res) => {
    const url = await upload(req.files[0].buffer, req.files[0].originalname);
    return res.json(url);
});

controller.showConsole = () => {
    // console.log('This is working !! ');
};

controller.emitEvent = handler(async (req, res) => {
    const { name, data } = req.body;
    socket.notificationSocket.emit(name, data);
    return res.json({
        message: 'Emitted !! ',
    });
});

module.exports = controller;
