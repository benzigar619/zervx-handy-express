const nodemailer = require('nodemailer');

const utils = {};

utils.sendMail = async () => {
    // let testAccount = await nodemailer.createTestAccount();
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: testAccount.user, // generated ethereal user
            pass: testAccount.pass, // generated ethereal password
        },
    });
    let info = await transporter.sendMail({
        from: '"Node Mailer 👻" <foo@example.com>', // sender address
        to: 'benzigar619@gmail.com', // list of receivers
        subject: 'Error !!', // Subject line
        text: 'Hello world?', // plain text body
        html: '<b>There is some problem in zervx !! </b>', // html body
    });
    console.log('Message sent: %s', info.messageId);
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
};

utils.sendMail();

module.exports = utils;
