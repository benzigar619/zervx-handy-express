const handler = require('express-async-handler');
const Officer = require('../models/officer.model');
const mongoose = require('mongoose');
const { formatDistance } = require('date-fns');

const controller = {};

controller.getOfficerDetails = handler(async (req, res) => {
    if (!req.query.number)
        throw '422|number key required in URL, pass it like [URL:PORT]/api/handy/officers/getOfficerDetails?number=9791442121';
    const officer = await Officer.findOne({
        'phone.number': req.query.number,
    }).lean();
    if (!officer) throw '401|Officer not found';
    else
        res.json({
            ...officer,
            locationUpdatedTime: formatDistance(
                new Date(officer.locationUpdatedTime),
                new Date(),
                { addSuffix: true }
            ),
        });
});
module.exports = controller;
