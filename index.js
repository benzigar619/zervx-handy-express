const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const handler = require('express-async-handler');
const compression = require('compression');
const multer = require('multer');

const upload = multer();
const app = express();

const config = require('./config');

// Utils
const { connectSocket } = require('./utils/socket.utils');
const errorUtil = require('./utils/errors.utils');
const initiateCron = require('./utils/crons.utils');
const dbUtil = require('./utils/db.utils');

const checkDB = dbUtil.checkDB;

const http = require('http').Server(app);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use(compression());
app.use(upload.any());

// Logging
app.use(
    morgan(':status\t:method\t:response-time ms\t:url', {
        skip: function (req, res) {
            return res.statusCode === 204;
        },
    })
);

app.get('/favicon.ico', function (req, res) {
    res.status(204);
    res.end();
});

// Routes
if (
    config.ENVIRONMENT === 'development' ||
    (config.ENVIRONMENT === 'production' && config.SERVICE === 'ALL')
) {
    connectSocket(config.SOCKET_PORT);
    initiateCron();
    app.use('/api/handy/users', require('./routes/users.routes'));
    app.use('/api/handy/admins', require('./routes/admin.routes'));
    app.use('/api/handy/officers', require('./routes/officers.routes'));
    app.use('/api/handy/securities', require('./routes/security.routes'));
    app.use('/api/handy/categories', require('./routes/categories.routes'));
}

// Not Found
app.use(
    '*',
    handler((req, res) => {
        throw '404|Not Found';
    })
);

// Error Handling
app.use((err, req, res, next) => errorUtil.handlerError(err, req, res));

// Staring the server
const start = async () => {
    // Mongo Connect
    await mongoose.connect(config.MONGO, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    });
    console.table(config);
    await checkDB();
    http.listen(config.PORT, () =>
        console.log(`Server running in ${config.PORT}`)
    );
};

start();
