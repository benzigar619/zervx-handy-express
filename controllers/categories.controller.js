const handler = require('express-async-handler');

const Category = require('../models/categories.models');
const DBUtils = require('../utils/db.utils');

const controller = {};

// Response Structures

controller.addCategory = handler(async (req, res) => {
    const updateObj = {
        name: req.body.name,
        level: req.body.level,
        categoryType: req.body.categoryType,
        subCategoriesAvailable: req.body.subCategoriesAvailable,
        subCategories: req.body.subCategories,
        parentLevels: req.body.parentLevels || [],
        questions: req.body.questions || [],
    };
    let category = {};
    if (req.body._id) {
        if (!DBUtils.isMongoID(req.body._id)) throw '400|Not a Valid Mongo ID';
        category = await Category.findById(req.body._id);
        if (!category) throw '401|Category ID Not Found';
        else
            category = await Category.findOneAndUpdate(
                {
                    _id: req.body._id,
                },
                { $set: updateObj },
                { new: true }
            );
    } else {
        const ifCategoryNameExists = await Category.findOne({
            name: req.body.name,
        });
        if (ifCategoryNameExists) throw '400|Category Name already exists';
        category = await Category.create(updateObj);
    }
    return res.json(category);
});

controller.getAllCategories = handler(async (req, res) => {
    const categories = (
        await Category.find({ categoryType: 'main' }).lean()
    ).map((each) => Category.structure(each));
    return res.json(categories);
});

controller.getSubCategories = handler(async (req, res) => {
    // Check Valid ID
    if (!DBUtils.isMongoID(req.body.id)) throw '401|Not a Valid ID';
    const category = Category.structure(await Category.findById(req.body.id));
    if (!category) return res.json([]);
    const categories = (
        await Category.find({
            _id: category.subCategories,
        })
    ).map((each) => Category.structure(each));
    return res.json(categories);
});

controller.getCategory = handler(async (req, res) => {
    if (!DBUtils.isMongoID(req.body.id)) throw '400|Not a Valid ID';

    let query = {};
    query = await Category.findById(req.body.id)
        .populate('subCategories')
        .populate('parentLevels.referId');
    const category = {
        ...Category.structure(query),
        subCategories: query.subCategories.map((each) =>
            Category.structure(each)
        ),
    };
    if (!category) throw '401|Category Not Found';
    else return res.json(category);
});

controller.check = handler(async (req, res) => {
    const query = [{}].map((each) => Category.structure(each));
    return res.json(query);
});

module.exports = controller;
