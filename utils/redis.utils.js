const Redis = require('ioredis');

const config = require('../config');

const helper = {};

const redis = new Redis(config.REDIS);

helper.get = async (key) => {
    const data = await redis.get(key);
    return data ? JSON.parse(data) : null;
};

helper.set = async (key, value) => {
    await redis.set(key, JSON.stringify(value));
};

module.exports = helper;
