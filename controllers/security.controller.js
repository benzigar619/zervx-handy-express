const handler = require('express-async-handler');
const Security = require('../models/security.models');

const DBUtil = require('../utils/db.utils');

const controller = {};

controller.getSecurityDetails = handler(async (req, res) => {
    if (req.query.id || req.query.ticketId) {
        if (req.query.id) {
            if (!DBUtil.isMongoID(req.query.id)) throw 'Invalid ID';
            const security = await Security.findById(req.query.id).lean();
            if (!security) throw 'No Security data found for given ID';
            else return res.json(security);
        } else if (req.query.ticketId) {
            const tickets = await Security.find({
                ticketId: req.query.ticketId,
            }).lean();
            return res.json(tickets);
        }
        return res.json({
            message: 'working !!',
        });
    } else throw 'id or ticketId required !! ';
});

module.exports = controller;
