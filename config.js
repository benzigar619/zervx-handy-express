const dotenv = require('dotenv');
dotenv.config();

const config = {};

// Server Start
config.PORT = process.env.PORT || 3001;
config.MONGO = process.env.MONGO || 'mongodb://165.232.188.66:27017/rays';
config.REDIS = process.env.REDI || 'redis://localhost:6379';
config.ENVIRONMENT = process.env.NODE_ENV || 'development';
config.SERVICE = process.env.SERVICE || 'ALL';
config.SOCKET_PORT = process.env.SOCKET_PORT || 5001;

module.exports = config;
