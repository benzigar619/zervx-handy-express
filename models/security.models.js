const mongoose = require('mongoose');

const securitySchema = new mongoose.Schema({
    firstName: String,
});

module.exports = mongoose.model('securityescorts', securitySchema);
