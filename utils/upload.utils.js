const aws = require('aws-sdk');
const Admin = require('../models/admin.models');

const utils = {};

utils.upload = (file, fileName) => {
    return new Promise(async (resolve, reject) => {
        const config = await Admin.config();
        const spaces = config.data.spaces;
        if (
            spaces &&
            spaces.spacesKey &&
            spaces.spacesSecret &&
            spaces.spacesEndpoint &&
            spaces.spacesBucketName &&
            spaces.spacesBaseUrl &&
            spaces.spacesObjectName
        ) {
            const spacesKey = spaces.spacesKey;
            const spacesSecret = spaces.spacesSecret;
            const spacesEndpoint = new aws.Endpoint(spaces.spacesEndpoint);

            const s3 = new aws.S3({
                endpoint: spacesEndpoint,
                accessKeyId: spacesKey,
                secretAccessKey: spacesSecret,
            });

            const fileContents = file;
            const params = {
                ACL: 'public-read',
                Bucket: spaces.spacesBucketName,
                Key: spaces.spacesObjectName + '/' + fileName,
                Body: fileContents,
            };
            s3.putObject(params, function (err, data) {
                if (err) {
                    console.log(err, err.stack);
                    resolve('');
                } else {
                    resolve(
                        spaces.spacesBaseUrl +
                            '/' +
                            spaces.spacesObjectName +
                            '/' +
                            fileName
                    );
                }
            });
        } else {
            console.log('Spaces key missing !! ');
            resolve('');
        }
    });
};

module.exports = utils;
