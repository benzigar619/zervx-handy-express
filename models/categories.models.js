const mongoose = require('mongoose');
const constantUtils = require('../utils/constant.utils');

const categorySchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true,
            default: '',
            unique: true,
        },
        parentLevels: [
            {
                level: {
                    type: Number,
                    required: true,
                },
                referId: {
                    type: mongoose.SchemaTypes.ObjectId,
                    ref: 'categories',
                    required: true,
                },
            },
        ],
        level: {
            type: Number,
            required: true,
            default: 1,
        },
        image: {
            type: String,
            trim: true,
            default: '',
        },
        categoryType: {
            type: String,
            enum: [constantUtils.MAIN, constantUtils.SUB],
            required: true,
            default: constantUtils.MAIN,
        },
        subCategoriesAvailable: {
            type: Boolean,
            required: true,
            default: false,
        },
        package: {
            type: Boolean,
            required: true,
            default: false,
        },
        status: {
            type: Boolean,
            required: true,
            default: true,
        },
        subCategories: [mongoose.SchemaTypes.ObjectId],
        questions: [
            {
                questionType: {
                    type: String,
                    required: true,
                    default: constantUtils.TEXTBOX,
                },
                text: {
                    type: String,
                    required: true,
                },
                options: [
                    {
                        type: String,
                        required: true,
                    },
                ],
            },
        ],
        bodyContent: [],
    },
    {
        versionKey: false,
        timestamps: true,
    }
);

categorySchema.pre('find', function () {
    this.lean();
});

categorySchema.pre('findById', function () {
    this.lean();
});

categorySchema.pre('findOne', function () {
    this.lean();
});

const Category = mongoose.model('categories', categorySchema, 'azcategories');

Category.structure = (each) => ({
    _id: each._id || null,
    name: each.name || null,
    level: each.level || null,
    parentLevels:
        (each.parentLevels &&
            each.parentLevels.map((each) => ({
                level: each.level || null,
                referId: each.referId || null,
            }))) ||
        [],
    image: each.image || '',
    categoryType: each.categoryType || null,
    subCategoriesAvailable: each.subCategoriesAvailable || false,
    package: each.package || false,
    subCategories: each.subCategories || [],
    questions:
        (each.questions &&
            each.questions.map((each) => ({
                _id: each._id || null,
                text: each.text || '',
                questionType: each.questionType || constantUtils.TEXTBOX,
                options: each.options || [],
            }))) ||
        [],
    bodyContent: each.bodyContent || [],
    status: each.status || true,
    createdAt: each.createdAt || null,
    updatedAt: each.updatedAt || null,
});

module.exports = Category;
