const handler = require('express-async-handler');
const User = require('../models/user.model');
const mongoose = require('mongoose');
const { formatDistance } = require('date-fns');

const controller = {};

controller.getUserDetails = handler(async (req, res) => {
    if (!req.query.number)
        throw '422|number key required in URL, pass it like [URL:PORT]/api/handy/users/getUserDetails?number=9791442121';
    const user = await User.findOne({
        'phone.number': req.query.number,
    }).lean();
    if (!user) throw 'User not found';
    else
        res.json({
            ...user,
            locationUpdatedTime: formatDistance(
                new Date(user.locationUpdatedTime),
                new Date(),
                { addSuffix: true }
            ),
        });
});
module.exports = controller;
