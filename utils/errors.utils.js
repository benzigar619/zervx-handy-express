const chalk = require('chalk');
const utils = {};

utils.validateBody = (schema) => (req, res, next) => {
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true, // remove unknown props
    };
    const { error, value } = schema.validate(req.body, options);
    if (error) {
        res.status(422).json({
            code: 422,
            message: error.details.map((each) => each.message),
        });
    } else {
        req.body = value;
        next();
    }
};

utils.handlerError = (err, req, res) => {
    // Draw Line for Serious Error
    if (err.message)
        console.log(chalk.redBright('-'.repeat(process.stdout.columns)));
    // If User Info Available
    if (err) {
        if (req?.user?.data?.phone?.number)
            console.log(
                chalk.cyan(
                    req.user.data.phone.code + ' ' + req.user.data.phone.number
                )
            );
    }
    // Console Error
    if (err.message) {
        if (Object.keys(req.body).length > 0)
            console.log(JSON.stringify(req.body, null, 2));
        console.log(
            chalk.redBright(err.stack) + '\t' + chalk.redBright(req.path)
        );
    } else
        console.log(
            chalk.yellow(err.includes('|') ? err.split('|')[1] : err) +
                '\t' +
                chalk.yellow(req.path)
        );
    // Response Error
    if (err.message) res.status(500).json({ code: 500, message: err.message });
    else
        res.status(err.includes('|') ? err.split('|')[0] : 500).json({
            code: err.includes('|') ? err.split('|')[0] : 500,
            message: err.includes('|') ? err.split('|')[1] : err,
        });
};

module.exports = utils;
