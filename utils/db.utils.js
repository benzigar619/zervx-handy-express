const chalk = require('chalk');

const Admin = require('../models/admin.models');
const Category = require('../models/categories.models');

const utils = {};

utils.checkDB = async () => {
    const general = await Admin.config();
    if (!general) console.log(chalk.bgRed('General Settings Not Found'));
    if (!general.data.spaces)
        console.log(chalk.bgRed('Spaces key missing in General Settings'));

    // Category Keys Check
    // const category = await Category.find({
    //     package: { $exists: false },
    // });
    // if (category.length() > 0) {
    //     console.log(chalk.bgGreen('Fixing Keys in azcategories'));
    //     await Category.updateMany(
    //         {
    //             package: { $exists: false },
    //         },
    //         {
    //             $set: {
    //                 package: false,
    //             },
    //         }
    //     );
    // }
};

utils.isMongoID = (value) => value.match(/^[0-9a-fA-F]{24}$/);

module.exports = utils;
