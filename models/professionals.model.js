const mongoose = require("mongoose");

const professionalSchema = new mongoose.Schema({
    firstName: String,
});

module.exports = mongoose.model("professionals", professionalSchema);
