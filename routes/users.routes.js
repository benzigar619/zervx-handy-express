const express = require('express');
const controller = require('../controllers/users.controller');
const router = express.Router();

router.get('/getUserDetails', controller.getUserDetails);

module.exports = router;
