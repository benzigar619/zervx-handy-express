const Joi = require('joi');
module.exports = {
    emitEvent: Joi.object({
        name: Joi.string().required(),
        data: Joi.object().required(),
    }),
};
