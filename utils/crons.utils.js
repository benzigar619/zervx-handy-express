const schedule = require('node-schedule');
const constantUtils = require('../utils/constant.utils.js');

const adminController = require('../controllers/admin.controller');

const startJobs = () => {
    console.log('CRON JOBS Activated');

    // Every Minute
    schedule.scheduleJob(constantUtils.CRON_EVERY_MINUTE, () => {
        adminController.showConsole();
    });
};

module.exports = startJobs;
