const Joi = require('joi');
module.exports = {
    addCategories: Joi.object({
        _id: Joi.string(),
        name: Joi.string().required().trim().lowercase(),
        level: Joi.number().required(),
        categoryType: Joi.string().required(),
        subCategoriesAvailable: Joi.boolean().required(),
        subCategories: Joi.array().required(),
        parentLevels: Joi.array()
            .items({
                level: Joi.number().required(),
                referId: Joi.string(),
            })
            .required(),
        previousLevel: Joi.string().default(''),
        questions: Joi.array().items({
            text: Joi.string().required(),
        }),
    }),
    getCategory: Joi.object({
        id: Joi.string().required(),
    }),
};
