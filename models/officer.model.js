const mongoose = require('mongoose');

const OfficerSchema = new mongoose.Schema({
    firstName: String,
});

module.exports = mongoose.model('officers', OfficerSchema);
