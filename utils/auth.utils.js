const CryptoJS = require('crypto-js');
const handler = require('express-async-handler');
const Admin = require('../models/admin.models');

const utils = {};

utils.hashSecret = (password) =>
    CryptoJS.enc.Hex.stringify(CryptoJS.SHA256(password));

const secret = utils.hashSecret(
    'my-32-character-ultra-secure-and-ultra-long-secret'
);

utils.base64Encode = (rawString) => {
    const wordArray = CryptoJS.enc.Utf8.parse(rawString);
    return CryptoJS.enc.Base64.stringify(wordArray)
        .replace('=', '')
        .replace('=', '');
};

utils.base64Decode = (base64String) => {
    try {
        const wordArray = CryptoJS.enc.Base64.parse(base64String);
        return CryptoJS.enc.Utf8.stringify(wordArray);
    } catch (error) {
        throw '401|Invalid Token';
    }
};

utils.decodeToken = (req) => {
    let token = req.headers.authorization;
    if (!token) throw '401|Token Required';
    if (!token.includes('Bearer ') || token.split(' ').length === 1)
        throw '401|Invalid Token';
    if (token.split(' ').length === 1) throw '401|Invalid Token';
    token = token.split(' ')[1];
    token = utils.base64Decode(token);
    const [signature, payload] = token.split(':').filter((d) => d.length > 0);
    const base64Signature = utils.base64Encode(
        CryptoJS.HmacSHA256(payload, secret)
    );

    if (signature === base64Signature) {
        const _payload = JSON.parse(utils.base64Decode(payload));
        return _payload;
    }

    throw '401|Invalid Token';
};

utils.adminTokenVerify = handler(async (req, res, next) => {
    const _payload = utils.decodeToken(req);
    const userData = await Admin.findOne({
        _id: _payload._id,
        'data.accessToken': req.headers.authorization.split(' ')[1],
    });
    if (!userData) throw '401|Invalid Token';
    req.user = userData;

    next();
});

module.exports = utils;
