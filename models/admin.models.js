const mongoose = require('mongoose');

const adminSchema = new mongoose.Schema(
    {
        name: String,
        data: Object,
    },
    {
        versionKey: false,
    }
);

const Admin = mongoose.model('admins', adminSchema);

Admin.config = function () {
    return this.findOne({ name: 'GENERALSETTING' });
};

module.exports = Admin;
