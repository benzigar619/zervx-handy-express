const express = require('express');
const controller = require('../controllers/officers.controller');
const router = express.Router();

router.get('/getOfficerDetails', controller.getOfficerDetails);

module.exports = router;
