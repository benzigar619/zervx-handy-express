const express = require('express');

const controller = require('../controllers/admin.controller');
const validation = require('../validations/admin.validation');

const { validateBody } = require('../utils/errors.utils');
const { adminTokenVerify } = require('../utils/auth.utils');
const config = require('../config');
const router = express.Router();

router.get('/getConfig', controller.getConfig);

// Auth Enabled
router.post('/getAllNames', adminTokenVerify, controller.getAllNames);
router.post('/getAllOperators', adminTokenVerify, controller.getAllOperators);
router.post('/getAllDevelopers', adminTokenVerify, controller.getAllDevelopers);
router.post('/getAllAdmins', adminTokenVerify, controller.getAllAdmin);
router.post('/getAllHubs', adminTokenVerify, controller.getAllHubs);
router.post('/getAllLanguages', adminTokenVerify, controller.getAllLanguages);
router.post('/getAllDocuments', adminTokenVerify, controller.getAllDocuments);

// Development
if (config.ENVIRONMENT === 'development') {
    router.post('/imageUpload', controller.imageUpload);
    router.post('/emitEvent', validateBody(validation.emitEvent), controller.emitEvent);
}

module.exports = router;
