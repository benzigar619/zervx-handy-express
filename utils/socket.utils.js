const config = require('../config');

const { format } = require('date-fns');

const notificationSocket = require('../sockets/notification.socket');

const utils = {};

let serverConnections = 0;
let notificationConnections = 0;
let userConnections = 0;

utils.notificationSocket = {};
utils.professionalSocket = {};
utils.userSocket = {};
utils.defaultSocket = {};

utils.io = {};

utils.socketConsole = (socket, msg = '') => {
    console.log(
        'SOCKET : ' +
            msg +
            format(new Date(socket.handshake.time), 'p') +
            ' ' +
            socket.id
    );
};

const notificationSocketConnect = (ioConnect) => {
    const io = ioConnect.of('/notifications');

    utils.notificationSocket = io;

    io.on('connection', function (socket) {
        // Connect
        if (socket.handshake.query.type === 'SERVER')
            utils.socketConsole(
                socket,
                ++serverConnections + ' SERVER CONNECTED '
            );
        else
            utils.socketConsole(
                socket,
                ++notificationConnections + ' NOTIFICATION CONNECTED '
            );
        socket.onAny((event, ...args) => {
            utils.socketConsole(socket, `Client Emitted : ${event} `);
        });

        // Sockets
        notificationSocket(socket);

        // Disconnect
        socket.on('disconnect', function () {
            if (socket.handshake.query.type === 'SERVER')
                utils.socketConsole(
                    socket,
                    --serverConnections + ' SERVER DISCONNECTED '
                );
            else
                utils.socketConsole(
                    socket,
                    --notificationConnections + ' NOTIFICATION DISCONNECTED '
                );
        });
    });
};

const userSocketConnect = (ioConnect) => {
    const io = ioConnect.of('/users');
    utils.userSocket = io;

    io.on('connection', function (socket) {
        // Connect
        if (socket.handshake.query.type === 'SERVER')
            utils.socketConsole(
                socket,
                ++serverConnections + ' SERVER CONNECTED '
            );
        else
            utils.socketConsole(socket, ++userConnections + ' USER CONNECTED ');
        socket.onAny((event, ...args) => {
            utils.socketConsole(socket, `Client Emitted : ${event} `);
        });
        // Sockets
        notificationSocket(socket);
        // Disconnect
        socket.on('disconnect', function () {
            if (socket.handshake.query.type === 'SERVER')
                utils.socketConsole(
                    socket,
                    --serverConnections + ' SERVER DISCONNECTED '
                );
            else
                utils.socketConsole(
                    socket,
                    --userConnections + ' USER DISCONNECTED '
                );
        });
    });
};

utils.connectSocket = (port) => {
    const io = require('socket.io')(port, {
        transports: ['websocket'],
    });
    console.log('Socket running in ' + port);
    const redisAdapter = require('socket.io-redis');

    utils.defaultSocket = io;

    // Socket with Redis
    io.adapter(redisAdapter(config.REDIS));

    // IO Connection
    notificationSocketConnect(io);
    userSocketConnect(io);

    utils.connectivityCheck();
};

utils.connectivityCheck = () => {
    const io = require('socket.io-client');
    const notificationSocket = io(
        `ws://localhost:${config.SOCKET_PORT}/notifications`,
        {
            transports: ['websocket'],
            query: {
                type: '',
            },
        }
    );
    const userSocket = io(`ws://localhost:${config.SOCKET_PORT}/users`, {
        transports: ['websocket'],
        query: {
            type: '',
        },
    });
};

module.exports = utils;
